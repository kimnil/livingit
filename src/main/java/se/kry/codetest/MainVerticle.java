package se.kry.codetest;

import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.List;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import se.kry.codetest.dao.ServiceDao;
import se.kry.codetest.dao.SqliteServiceDao;
import se.kry.codetest.model.Service;

public class MainVerticle extends AbstractVerticle {

    // Port to listen to
    private static final int PORT = 8080;

    @Override
    public void start(Future<Void> startFuture) {

        DBConnector connector = new DBConnector(vertx);
        ServiceDao serviceDao = new SqliteServiceDao(connector);

        BackgroundPoller poller = new BackgroundPoller(serviceDao, vertx, 100 * 60);
        poller.init();

        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());

        setRoutes(router, serviceDao);
        vertx
                .createHttpServer()
                .requestHandler(router)
                .listen(PORT, result -> {
                    if (result.succeeded()) {
                        System.out.println("KRY code test service started, listening on " + PORT);
                        startFuture.complete();
                    } else {
                        startFuture.fail(result.cause());
                    }
                });
    }

    private void setRoutes(Router router, ServiceDao serviceDao){
        router.route("/*").handler(StaticHandler.create());

        // Get all
        router.get("/service").handler(req -> {

            Future<Collection<Service>> all = serviceDao.getAll();
            Future<List<JsonObject>> fServicesAsJson = all.compose(services ->
                    Future.succeededFuture(services.stream()
                            .map(Service::jsonEncode)
                            .collect(toList())));

            fServicesAsJson.setHandler(event -> {
                if (event.succeeded()) {
                    List<JsonObject> jsonServices = event.result();
                    req.response()
                            .putHeader("content-type", "application/json")
                            .end(new JsonArray(jsonServices).encode());
                } else {
                    System.err.println("Something went wrong reading all services");
                    req.fail(505);
                }
            });

        });

        // Add
        router.post("/service").handler(req -> {
            JsonObject jsonBody = req.getBodyAsJson();

            Service service = Service.deserialize(jsonBody);

            serviceDao.add(service)
                    .setHandler(event -> {
                        returnFailOrSuccess(req, event);
                    });
        });

        // Delete by id
        router.deleteWithRegex("\\/service\\/(?<id>\\d+)").handler(req -> {
            String stringId = req.request().getParam("id");

            int id = Integer.parseInt(stringId);

            serviceDao.delete(id)
                    .setHandler(ar -> returnFailOrSuccess(req, ar));

        });

        // Delete all (useful when testing)
        router.delete("/service").handler(req -> {
            serviceDao.getAll()
                    .compose(result -> CompositeFuture.all(result.stream()
                            .map(Service::getId).map(serviceDao::delete).collect(toList())))
                    .setHandler(event -> {
                        returnFailOrSuccess(req, event);

                    });
        });
    }

    private void returnFailOrSuccess(RoutingContext req, AsyncResult<?> event) {
        final JsonObject payload = new JsonObject();
        if (event == null || !event.succeeded()) {
            payload.put("result", "failed");
            payload.put("cause", event != null ? event.toString() : "unknown error");

            req.response()
                    .setStatusCode(500)
                    .putHeader("content-type", "text/json")
                    .end(payload.toString());
        } else {
            payload.put("result", "success");

            if (event.result() != null) {
                payload.put("output", event.result().toString());
            }

            req.response()
                    .setStatusCode(200)
                    .putHeader("content-type", "text/json")
                    .end(payload.toString());
        }
    }

}



