package se.kry.codetest.model;

import java.util.Objects;

import io.vertx.core.json.JsonObject;

/**
 * Represents a service to poll.
 *
 * Note: I would like to use lombok here but since some people hate it (and maybe you are one of them)
 *       I did not, I should have, but I didnt... :)
 */
public class Service {

    // todo: make url into its own type and validate its format
    private int id;
    private String createdAt;
    private final String url;
    // why bother storing this. should be removed from this entity and maybe make another one (TrackedService)
    // or something and only keep status in memory.
    private final String status;
    private final String name;

    private Service(int id, String url, String name, String status, String createdAt) {
        this.id = id;
        this.url = url;
        this.name = name;
        this.status = status;
        this.createdAt = createdAt;
    }

    private static Service of(String url, String name) {
       return new Service(-1, url, name, "not polled yet", null);
    }

    public static Service deserialize(JsonObject jsonBody) {
        String url = jsonBody.getString("url");
        String name = jsonBody.getString("name");
        return Service.of(url, name);
    }

    public static Service deserializeFromDb(JsonObject jsonBody) {
        int id = jsonBody.getInteger("id");
        String url = jsonBody.getString("url");
        String name = jsonBody.getString("name");
        String status = jsonBody.getString("status");
        String createdAd = jsonBody.getString("created_at");
        return new Service(id, url, name, status, createdAd);
    }

    public JsonObject jsonEncode() {
        return new JsonObject()
                .put("id", this.id)
                .put("url", this.url)
                .put("name", this.name)
                .put("status", this.status)
                .put("createdAt", this.createdAt);
    }


    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getStatus() {
        return this.status;
    }

    public void setCreatedAt(String timestamp) {
        this.createdAt = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Service service = (Service) o;
        return id == service.id &&
                Objects.equals(createdAt, service.createdAt) &&
                Objects.equals(url, service.url) &&
                Objects.equals(status, service.status) &&
                Objects.equals(name, service.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, createdAt, url, name);
    }

    public Service withStatus(String updatedStatus) {
        return new Service(this.id, this.url, this.name, updatedStatus, this.createdAt);
    }
}
