package se.kry.codetest;

import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;
import se.kry.codetest.dao.ServiceDao;
import se.kry.codetest.model.Service;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@SuppressWarnings("FieldCanBeLocal") // clearer this way
class BackgroundPoller {

    private final ServiceDao serviceDao;
    private final Set<Service> trackedServices = new HashSet<>();
    private final WebClient webClient;
    private final Vertx vertx;

    private static final int POLL_PORT = 80;
    private static long DEFAULT_TIMEOUT_MS = 2_000;

    private final int frequencyInMs;

    BackgroundPoller(final ServiceDao serviceDao, Vertx vertx, int frequencyInMs) {
        Objects.requireNonNull(serviceDao);
        Objects.requireNonNull(vertx);

        this.vertx = vertx;
        this.serviceDao = serviceDao;
        this.webClient = WebClient.create(vertx);
        this.frequencyInMs = frequencyInMs;
    }

    /**
     * Start polling.
     */
    void init() {
        serviceDao.registerListener(getListener());
        // Every N:th second.
        // Todo: this should be spread out over the entire period to avoid bursts.
        vertx.setPeriodic(frequencyInMs, ignore -> this.pollServices());
    }

    private void pollServices() {
        trackedServices.forEach(this::poll);
    }

    private void poll(Service service) {
        String url = service.getUrl();

        webClient
                .get(POLL_PORT, url, "/")
                .timeout(DEFAULT_TIMEOUT_MS)
                .send(req -> {
                    Service updatedService;
                    if (req.succeeded()) {
                        System.out.println(service.getName() + " is ok!");
                        updatedService = service.withStatus("Ok!");
                    } else {
                        updatedService = service.withStatus("Not reachable!");
                        System.out.println(service.getName() + " is not reachable!");
                    }

                    if (!updatedService.equals(service)) {
                        // remove status from db to reduce db load
                        serviceDao.update(updatedService);
                    }
                });
    }

    /*
        Using event bus this would pretty much be the same.
        We would probably have onServiceAdded(ServiceAdded) and onServiceRemoved(ServiceRemoved).
     */
    private ServiceDao.ServiceListener getListener() {
        return new ServiceDao.ServiceListener() {
            @Override
            public void serviceAdded(Service service) {
                System.out.println("tracking new service " + service.toString());
                trackedServices.add(service);
            }

            @Override
            public void serviceDeleted(int id) {
                boolean b = trackedServices.removeIf(s -> s.getId() == id);
                if (b) {
                    System.out.println("no longer tracking " + id);
                }
            }
        };
    }
}
