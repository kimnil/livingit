package se.kry.codetest.dao;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;


import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import se.kry.codetest.DBConnector;
import se.kry.codetest.model.Service;

public class SqliteServiceDao implements ServiceDao {

    private final DBConnector dbHandle;
    private static final String TABLE_NAME = "service";
    private static final String SQL_GET_BY_URL = "SELECT * FROM %s WHERE url = '%s'";
    private static final String SQL_GET_ALL = "SELECT * FROM %s";
    private static final String SQL_INSERT_URL = "INSERT INTO %s (url,name,created_at,status) VALUES ('%s','%s','%s','%s');";
    private static final String SQL_UPDATE_STATUS = "UPDATE %s SET status='%s' WHERE id='%d'";
    private static final String SQL_DELETE_URL = "DELETE FROM %s WHERE id = '%d'";

    // I'm not happy with this. I'm pretty sure vertx has an event bus, but I think
    // this is faster for me as I don't know much about vertx.
    private final List<ServiceListener> listeners = new LinkedList<>();

    public SqliteServiceDao(DBConnector dbHandle) {
        Objects.requireNonNull(dbHandle, "'dbHandle' must not be null!");
        this.dbHandle = dbHandle;
    }

    @Override
    public Future<Service> getOne(final String url) {
        Objects.requireNonNull(url);

        // This is probably not a very good way to do this. Can't trust url here. Smells like SQL attacks.
        // Will come back to this if there is time.
        // Todo: clean up user input
        String sql = String.format(SQL_GET_BY_URL, TABLE_NAME, url);

        Future<ResultSet> query = dbHandle.query(sql);

        return query.compose(resultSet -> {
            if (resultSet.getNumRows() != 1) {
                return Future.failedFuture("No such service (url " + url + ")");
            } else {
                JsonObject next = resultSet.getRows().iterator().next();
                Service service = Service.deserializeFromDb(next);
                return Future.succeededFuture(service);
            }
        });
    }

    @Override
    public Future<Collection<Service>> getAll() {

        System.out.println("Reading all services");

        // again with the naked sql...
        String sql = String.format(SQL_GET_ALL, TABLE_NAME);
        Future<ResultSet> query = dbHandle.query(sql);

        query.otherwise(error -> {
            System.err.println("An error occoured while reading service");
            error.printStackTrace();
            return new ResultSet();
        });

        return query.compose(resultSet -> resultSet.getRows().stream()
                .filter(Objects::nonNull)
                .map(Service::deserializeFromDb)
                .collect(collectingAndThen(toList(), Future::succeededFuture)));
    }

    @Override
    public Future<Void> delete(int id) {
        System.out.println("Deleting service with id " + id);
        String sql = String.format(SQL_DELETE_URL, TABLE_NAME, id);
        Future<ResultSet> query = dbHandle.query(sql);

        return query.compose(r -> {
            listeners.forEach(listener -> listener.serviceDeleted(id));
            return Future.succeededFuture();
        });
    }

    // todo: update more than status
    @Override
    public Future<Void> update(Service updatedObject) {
        System.out.println("Updating service with id " + updatedObject.getId());
        String sql = String.format(SQL_UPDATE_STATUS, TABLE_NAME, updatedObject.getStatus(), updatedObject.getId());
        Future<ResultSet> query = dbHandle.query(sql);

        return query.compose(r -> Future.succeededFuture());
    }

    @Override
    public Future<Service> add(Service service) {
        service.setCreatedAt(getCurrentTimeString());
        System.out.println("Adding service" + service.toString());
        String sql = String.format(SQL_INSERT_URL, TABLE_NAME,
                service.getUrl(), service.getName(), service.getCreatedAt(), service.getStatus());

        Future<ResultSet> fQuery = dbHandle.query(sql);

        // Why does none of these work for fQuery?
        // Unexpected. I can compose but I was expecting these to fire as well.
        /*
        fQuery.map(result -> {
        });
        fQuery.otherwise(fail -> {
        });
        fQuery.setHandler(ar -> {
        });
        */

        return fQuery
                .compose(res -> getOne(service.getUrl()))
                .compose(newService -> { listeners.forEach(l -> l.serviceAdded(newService));
                    return Future.succeededFuture(newService);
                });
    }

    private String getCurrentTimeString() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    @Override
    public void registerListener(ServiceListener listener) {
        this.listeners.add(listener);

        // Initial push
        getAll().setHandler(event -> {
            if (event.succeeded()) {
                event.result().forEach(listener::serviceAdded);
            }
        });
    }

    @Override
    public void unregisterListener(ServiceListener listener) {
        this.listeners.remove(listener);
    }

}
