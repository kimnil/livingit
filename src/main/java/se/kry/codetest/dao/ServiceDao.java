package se.kry.codetest.dao;


import java.util.Collection;

import io.vertx.core.Future;
import se.kry.codetest.model.Service;

/**
 */
public interface ServiceDao {

    Future<Service> getOne(String url);

    Future<Collection<Service>> getAll();

    Future<Void> delete(int id);

    /**
     * Returns old object.
     */
    Future<Void> update(Service updatedObject);

    Future<Service> add(Service service);

    // todo: Listener stuff should go into separate interface.
    void registerListener(ServiceListener listener);

    void unregisterListener(ServiceListener listener);

    interface ServiceListener {
        void serviceAdded(Service service);

        void serviceDeleted(int id);
    }
}
