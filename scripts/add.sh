#!/bin/bash
set -e

if [ -z "${1}" ]
then
      echo "Usage: ./add.sh dn.json"
      exit 1
fi


curl -X POST -d "@${1}" "localhost:8080/service/" | jq .
