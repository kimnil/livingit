const listContainer = document.querySelector('#service-list');
let servicesRequest = new Request('/service');
fetch(servicesRequest)
    .then(function(response) { return response.json(); })
    .then(function(serviceList) {
        serviceList.forEach(service => {
            var li = renderService(service);
        listContainer.appendChild(li);
    });
    });

function renderService(service) {
    var li = document.createElement("li");
    var text = service.name + ' (' + service.url + '), added: ' + service.createdAt + ": Current status: " + service.status;

    // this is hacky but it adds some colour
    function getClassName() {
        if (service.status === "Ok!") {
            return "ok";
        } else if (service.status === "Not reachable!") {
            return "fail";
        } else {
            return "pending";
        }
    }

    li.className += getClassName();
    li.appendChild(document.createTextNode(text));


    var removeButton = document.createElement("input");
    removeButton.type = "button";
    removeButton.value = "Untrack";

    li.appendChild(removeButton);

    li.onclick = evt => {
        fetch('/service/' + service.id, {
            method: 'delete',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            }
        }).then(res=> location.reload());
    };
    return li;
}

const saveButton = document.querySelector('#post-service');
saveButton.onclick = evt => {

    let url = document.querySelector('#url').value;
    let name = document.querySelector('#name').value;

    fetch('/service', {
        method: 'post',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({url: url, name: name})
    }).then(res=> location.reload());
}